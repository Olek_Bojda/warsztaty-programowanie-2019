# Warsztaty Programowanie 2019

## Przydatne funkcje
### Projekt #1
`HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);`

`HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);`

`HAL_Delay(200);`

`state = HAL_GPIO_ReadPin(B1_GPIO_Port,B1_Pin);`

### Projekt #2
```C
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_13)
	{
		state = !state;
	}
}
```

### Projekt #3
```C
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	value = HAL_ADC_GetValue(&hadc1);
	flag = 1;
}
```

`HAL_ADC_Start_IT(&hadc1);`

`printf("ADC Value: %i\n\r",value);`


### Projekt #4

```C
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_13)
	{
		duty += 20;

		if (duty >= 100)
		{
			duty = 0;
		}
	}
}
```

`HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);`

`__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,duty);`


### Projekt #5
```C
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Instance == htim17.Instance)
	{
		HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
	}
}
```

`HAL_TIM_Base_Start_IT(&htim17);`


### Inne przydatne funkcje
```C
int _write(int file, char *ptr, int len) 
{
	HAL_UART_Transmit(&huart2, (uint8_t *) ptr, len, 50);
	return len;
}
```